 # **Daily Report (2023/07/17)**  

## O
1. Code Review：Before writing code, it is important to clarify the requirements. If the code is written well and the requirements are incorrect, everything should be cleared to zero. Using TDD development projects, Code refactoring is very important.
2. Command Pattern：Command pattern is a data-driven design mode. Requests are wrapped in objects in the form of commands and passed to the calling object. Call the object to find a suitable object that can handle the command, and pass the command to the corresponding object, which executes the command.
3. Strategy Pattern：In the Strategy pattern, a series of algorithms or policies are defined, and each algorithm is encapsulated in an independent class so that they can replace each other. By using the Strategy pattern, you can select different algorithms according to your needs at runtime without modifying the client code.
4. Observer Pattern: The Observer pattern is a behavioral design pattern that defines a one to many dependency. When the state of an object changes, all its dependents will receive notification and be updated automatically.
5. Refactor: Having understood the basic requirements of Code refactoring, I have conducted Code refactoring in previous projects, but it is only limited to optimizing performance, ignoring code readability, etc.

## R
meaningful
## I
 I think refactor is very important in development.The most meaningful activity is refactor.
## D
  Spend more time and energy on Code refactoring in the future