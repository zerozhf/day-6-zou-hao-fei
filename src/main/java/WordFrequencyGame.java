import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public String countTheNumberOfEachWord(String inputStr) {
        try {
            List<WordInfo> wordInfoList = createWordListByInputStr(inputStr);
            Map<String, List<WordInfo>> wordMap = createInputListMapGroupingByWord(wordInfoList);
            wordInfoList = countTheNumberOfEachWord(wordMap);
            return generateWordQuantityInformation(wordInfoList);
        } catch (Exception e) {
            return  "Calculate Error";
        }
    }


    private Map<String, List<WordInfo>> createInputListMapGroupingByWord(List<WordInfo> wordInfoList) {
        return wordInfoList.stream()
                .collect(Collectors.groupingBy(WordInfo::getValue));
    }

    private List<WordInfo> createWordListByInputStr(String inputStr) {
        String KEY_BOARD_SPACE = "\\s+";
        return Arrays.stream(inputStr.split(KEY_BOARD_SPACE))
                .map((word) -> new WordInfo(word, 1))
                .collect(Collectors.toList());
    }

    private List<WordInfo> countTheNumberOfEachWord(Map<String, List<WordInfo>> wordMap) {
        return wordMap.entrySet().stream()
                .map(entry -> new WordInfo(entry.getKey(), entry.getValue().size()))
                .sorted(Comparator.comparingInt(WordInfo::getWordCount).reversed())
                .collect(Collectors.toList());
    }

    private String generateWordQuantityInformation(List<WordInfo> wordInfoList) {
        return wordInfoList.stream()
                .map(wordInfo -> wordInfo.getValue() + " " + wordInfo.getWordCount())
                .collect(Collectors.joining("\n"));
    }


}
